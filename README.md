## How start

```sh
1. python -m venv .venv
2. source .venv/bin/activate
3. pip install -r requirements.txt
4. python manage.py makemigrations
5. python manage.py migrate
6. python manage.py runserver

```

## API DOC

### Country - search

```sh
curl --location 'http://localhost:8000/api/countries?search=china' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNzA2MzcwOTU4LCJpYXQiOjE3MDYzNzA2NTgsImp0aSI6Ijk5NDljYzg5YzQzZjRiYzdhZmNhNmM4Yjg1NzNkN2FkIiwidXNlcl9pZCI6Mn0.ZroPKDvsHcdkBJmIyNr3xCsE9XI3Nc3eir1WCNxKfKw'

```

### Country

```sh
curl --location 'http://localhost:8000/api/countries' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNzA2MzcxNDMyLCJpYXQiOjE3MDYzNzExMzIsImp0aSI6IjQwNWU0YTljZTYzZTQ2NGNhMTFhMDA5YjhiYmEyZDBkIiwidXNlcl9pZCI6Mn0.AP7tKFgMlZdtXD1Qel6t_ruHkrUezHz2pvAxy3BZDKY'

```

### Category

```sh
curl --location 'http://localhost:8000/api/categories' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNzA2MzcxNDMyLCJpYXQiOjE3MDYzNzExMzIsImp0aSI6IjQwNWU0YTljZTYzZTQ2NGNhMTFhMDA5YjhiYmEyZDBkIiwidXNlcl9pZCI6Mn0.AP7tKFgMlZdtXD1Qel6t_ruHkrUezHz2pvAxy3BZDKY'

```


### Destination

```sh
curl --location 'http://localhost:8000/api/destination?search=6' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNzA2OTgyNTExLCJpYXQiOjE3MDYzNzc3MTEsImp0aSI6IjUxNDg1YjljMzRkNDQwMmRiM2I3YWM0NmZjYzNkZjAyIiwidXNlcl9pZCI6Mn0.281p9HZsmVDJTuwRdslVc1ok-P6Ie1AEed0P5W2siuE'

```

### Calculate

```sh
curl --location 'http://localhost:8000/api/calculate/' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNzA2MzczNzg3LCJpYXQiOjE3MDYzNzM0ODcsImp0aSI6IjJmODkzZDU0YmM3MjQzZDFiZWZiZjM4MDRiOTk3MzI3IiwidXNlcl9pZCI6Mn0.c3esRk501efqLGZPjYlOWI6DVhiUIWXpqjNYRkL1-2w' \
--data '{
    "country_id": 2,
    "category_id":2,
    "destination_id": "china",
    "weight": 2

}'

```

### Register

```sh
curl --location 'http://localhost:8000/api/register/' \
--form 'username="adminokta"' \
--form 'password="password12@"' \
--form 'password2="password12@"' \
--form 'email="oktatampan@gmail.com"'

```
### Login

```sh
curl --location 'http://localhost:8000/api/login/' \
--form 'username="adminokta"' \
--form 'password="password12@"'

```