from django.contrib import admin
from masterdata.models import Country, Category
from masterdata.admin_views import category_admin_views, country_admin_views

# Register your models here.
admin.site.register(Category, category_admin_views.CategoryAdminView)
admin.site.register(Country, country_admin_views.CountryAdminView)
