from django.contrib import admin


class CountryAdminView(admin.ModelAdmin):
    list_display = ('name', 'flag', 'currency', 'created_at', 'updated_at')
    
    fieldsets = [
        (
            'General', {
                'fields': (
                    'name',
                    'currency',
                )
            }
        ),
    ]

    def save_model(self, request, obj, form, change):
        qs = super().save_model(request, obj, form, change)
        obj.flag
        return 