from django.contrib import admin


class CategoryAdminView(admin.ModelAdmin):
    list_display = ('country', 'title', 'price_per_kilo', 'created_at', 'updated_at')
    
    fieldsets = [
        (
            'General', {
                'fields': (
                    'country',
                    'title',
                    'price_per_kilo',
                )
            }
        ),
    ]

    